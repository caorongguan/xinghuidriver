package com.xinghuidriver.owner.base;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.xinghuidriver.owner.R;
import com.xinghuidriver.owner.adapter.HomePageAdapter;
import com.xinghuidriver.owner.config.Constant;
import com.xinghuidriver.owner.view.NestedViewPager;

import java.util.ArrayList;
import java.util.List;


/**
 * create by 曹荣冠
 * on 2019/12/18
 */
public abstract class HomeBasePagerFragment extends BaseFragment {
    com.xinghuidriver.owner.view.TabLayout mTabLayout1;
    public NestedViewPager mViewPager;
    protected RelativeLayout mTitleBar;
    protected TextView mTitle;
    protected HomePageAdapter adapter;
    protected List<BaseFragment> mFragments;
    protected List<String> mTitles;

    @Override
    public int getLayout() {
        return R.layout.fragment_viewpager_new;
    }

    @Override
    public void initView() {
        mTabLayout1 = getViewById(R.id.main_TabLayout);
        mViewPager = getViewById(R.id.view_pager);
        mViewPager.setLocked(true);
        mTitleBar = getViewById(R.id.rl_title_bar);
        mTitle = getViewById(R.id.tv_title);
        mTitles = new ArrayList<>();
        mFragments = new ArrayList<>();
        setAdapter();
    }

    /**
     * init adapter
     */
    protected void setAdapter() {
        adapter = new HomePageAdapter(getChildFragmentManager(), createFragments());
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(createFragments().size());
        initMTableLayout();
    }

    public TextView[] tvList = new TextView[Constant.OWNER_TITLE.length];

    private void initMTableLayout() {
        mTabLayout1.setupWithViewPager(mViewPager, true);
        mTabLayout1.removeAllTabs();
        for (int i = 0; i < Constant.OWNER_TITLE.length; i++) {
            View view = LayoutInflater.from(activity).inflate(R.layout.navitab_layout, null);
            TextView tv = view.findViewById(R.id.nav_tv);
            ImageView im = view.findViewById(R.id.nav_iv);
            tvList[i] = tv;
            tv.setText(Constant.OWNER_TITLE[i]);
            im.setImageResource(Constant.OWNER_ICONS[i]);
            mTabLayout1.addTab(mTabLayout1.newTab().setCustomView(view));
        }
    }

    /**
     * create Fragment
     *
     * @return List<BaseFragment>
     */
    protected abstract List<BaseFragment> createFragments();

}
