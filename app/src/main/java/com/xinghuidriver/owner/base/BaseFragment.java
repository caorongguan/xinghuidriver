package com.xinghuidriver.owner.base;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;


import com.xinghuidriver.owner.utils.CommonUtils;

import java.util.HashMap;


/**
 * Created by caorongguan on 2019/5/6.
 */

public abstract class BaseFragment extends Fragment {
    public View rootView;
    protected FragmentActivity activity;
    protected boolean mIsFirstVisible = true;
    public BaseAct mAct;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {
        rootView = inflater.inflate(getLayout(), container, false);
        mAct = (BaseAct)getActivity();
        mContext = this.getContext();
        CommonUtils.setLog("rootView",rootView==null?"null":"not null");
        initView();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        boolean isVis = isHidden() || getUserVisibleHint();
        if (isVis && mIsFirstVisible) {
            mIsFirstVisible = false;
        }
    }



    /**
     * @return
     */
    public abstract int getLayout();

    /**
     * 初始化views
     */
    public abstract void initView();


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.activity = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (FragmentActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.activity = null;
    }

    @SuppressWarnings("unchecked")
    protected <T extends View> T getViewById(int id) {
        return (T) rootView.findViewById(id);
    }

    public void showToast(String msg) {
        CommonUtils.showToast(mContext, msg);
    }

    public void setText(TextView textView, String text) {
        CommonUtils.setText(textView, text);
    }

    public void setText(TextView textView, int text) {
        setText(textView, text + "");
    }

    public void setText(TextView textView, double text) {
        setText(textView, text + "");
    }

    public void jumpToFragment(Fragment fragment, int id) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(id, fragment).commit();
    }

    public void jumpToChildFragment(Fragment fragment, int id) {
        getChildFragmentManager().beginTransaction().replace(id, fragment).commit();
    }

    public void jumpToAct(Class activity) {
        Intent intent = new Intent(mAct, activity);
        mAct.startActivity(intent);
    }

    public void jumpToAct(Class activity, Bundle data) {
        Intent intent = new Intent(mAct, activity);
        intent.putExtra("data", data);
        mAct.startActivity(intent);
    }


    public <T> void addParams(HashMap<String, Object> map, String key, T value) {
        if (value != null && !TextUtils.isEmpty(value.toString())) {
            map.put(key, value);
        }
    }

    public void setStateHeight(View view){
        view.setPadding(0,getStatusHeight(activity), 0, 0);
    }

    public int getColorBase(int id) {
        return ContextCompat.getColor(activity, id);
    }

    /**
     * 获得状态栏的高度
     *
     * @param context
     * @return
     */
    public  int getStatusHeight(Context context) {
        int statusHeight = -1;
        try {
            Class<?> clazz = Class.forName("com.android.internal.R$dimen");
            Object object = clazz.newInstance();
            int height = Integer.parseInt(clazz.getField("status_bar_height")
                    .get(object).toString());
            statusHeight = context.getResources().getDimensionPixelSize(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusHeight;
    }

}
