package com.xinghuidriver.owner.base;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.xinghuidriver.owner.R;
import com.xinghuidriver.owner.utils.CommonUtils;
import com.xinghuidriver.owner.utils.CommonViewUtils;
import com.xinghuidriver.owner.utils.PermissionUtil;
import com.xinghuidriver.owner.utils.StatusBarUtils;
import com.xinghuidriver.owner.utils.img.PictureUtil;

import java.io.File;

import io.reactivex.annotations.NonNull;

/**
 * @Author : caorongguan
 * @Time : On 2021/6/14 21:19
 * @Description : BaseAct
 */
public abstract class BaseAct extends AppCompatActivity {
    protected Context mContext;
    public Bundle savedInstanceState;

    private TextView tvCommonTitle;
    private ImageView ivCommonLeft;
    private View contentView, commonTitle, divider;
    public LinearLayout title_ll;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        this.setContentView(R.layout.activity_base);
        mContext = this;
        initTint();
        LinearLayout llTitle = findViewById(R.id.ll_base_title);
        LinearLayout llContent = findViewById(R.id.ll_base_content);
        initTitleView(llTitle);
        initContentView(llContent);
        initViews();
    }

    /**
     * 初始化沉浸式
     */
    private void initTint() {
        StatusBarUtils.transparencyBar(this);
        StatusBarUtils.setDarkStatusIcon(this, true);
    }

    private void initTitleView(LinearLayout llTitle) {
        if (!needShowTitle()) return;
        llTitle.removeAllViews();
        commonTitle = getLayoutInflater().inflate(R.layout.layout_base_title, null);
        initCommonTitle();
        llTitle.addView(commonTitle);
    }

    private void initCommonTitle() {
        title_ll = commonTitle.findViewById(R.id.title_ll);
        title_ll.setPadding(0, getStatusHeight(), 0, 0);
        title_ll.setBackgroundColor(getTitleBackground());
        if (getTitleBackground() == ContextCompat.getColor(this, R.color.white)) {
            showStatusBarDark(true);//如果title 背景色为白色则状态栏显示暗色
        } else {
            showStatusBarDark(false); //如果title 背景色不为白色则状态栏显示亮色
        }
        tvCommonTitle = commonTitle.findViewById(R.id.tv_base_title);
        ivCommonLeft = commonTitle.findViewById(R.id.iv_base_title_left);
        divider = commonTitle.findViewById(R.id.divider_base);
    }

    private void initContentView(LinearLayout llContent) {
        contentView = LayoutInflater.from(mContext).inflate(getLayout(), null);
        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        } else {
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        contentView.setLayoutParams(layoutParams);
        llContent.addView(contentView);
    }


    public void setCommonTitle(String title) {
        if (tvCommonTitle != null) {
            tvCommonTitle.setText(CommonUtils.ifStringEmpty(title));
        }
    }

    public void setBackImgListener(View.OnClickListener listener) {
        if (ivCommonLeft != null) {
            CommonViewUtils.setOnClick(ivCommonLeft, v -> {
                if (listener != null) listener.onClick(v);
            });
        }
    }

    /**
     * 返回标题栏(非透明背景)
     *
     * @return view
     */
    protected View getTitleView() {
        return commonTitle;
    }

    public void showLine(boolean isV) {
        setG(divider, isV);
    }


    protected int getTitleBackground() {
        return ContextCompat.getColor(this, R.color.white);
    }

    /**
     * 是否需要显示标题(包括透明背景与非透明背景)
     *
     * @return true 需要 , false 不需要
     */
    protected boolean needShowTitle() {
        return true;
    }

    /**
     * Activity中若涉及多个Fragment页面管理
     * 调用 mBaseFragment.onBackPressed()
     * 以观察者模式监听所有Fragment的Back键
     **/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public abstract int getLayout();

    public abstract void initViews();


    /**
     * 设置沉浸栏 needShowTitle=false的时候一定要设置
     */
    public void setStatusHeight(View view) {
        if (view == null) return;
        if (!needShowTitle()) {
            view.setPadding(0, getStatusHeight(), 0, 0);
        }
    }

    public void jumpToAct(Class<?> cls) {
        Intent intent = new Intent(mContext, cls);
        startActivity(intent);
    }

    /**
     * 注意这个是activity中切换并非跳转共同依赖的activity
     */
    public void jumpToFragment(Fragment fragment, @IdRes int id) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(id, fragment);
        ft.commit();
    }

    public void addFragment(BaseFragment fragment, int id) {
        FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
        fm.add(id, fragment);
        fm.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //通过requestCode来识别是否同一个请求
        if (requestCode == PictureUtil.CAMERA_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                //用户同意，执行操作
                PictureUtil.openCamera(this, PictureUtil.CAMERA_REQUEST_CODE);
            } else {
                //用户不同意，向用户展示该权限作用
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, PermissionUtil.cameraPermission)) {
                    new AlertDialog.Builder(this)
                            .setMessage("拒绝该权限将无法使用相机功能")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ActivityCompat.requestPermissions(BaseAct.this,
                                            PictureUtil.cameraAndStorePermissionList,
                                            PictureUtil.CAMERA_REQUEST_CODE);
                                }
                            }).setNegativeButton("Cancel", null)
                            .create()
                            .show();

                }
            }
        } else if (requestCode == PictureUtil.GALLERY_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //用户同意，执行操作
                PictureUtil.openGallery(this, PictureUtil.CAMERA_REQUEST_CODE);
            } else {
                //用户不同意，向用户展示该权限作用
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    new AlertDialog.Builder(this)
                            .setMessage("拒绝该权限将无法使用相册功能")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ActivityCompat.requestPermissions(BaseAct.this,
                                            new String[]{PermissionUtil.storeReadPermission},
                                            PictureUtil.GALLERY_REQUEST_CODE);
                                }
                            }).setNegativeButton("Cancel", null)
                            .create()
                            .show();

                }
            }
        }
    }

    protected void setCallBackImage(Bitmap bitMap, File file) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PictureUtil.CAMERA_REQUEST_CODE || requestCode == PictureUtil.GALLERY_REQUEST_CODE) {
                PictureUtil.imageResult(this, requestCode, data, (Bitmap bitmap, File file) -> {
                    setCallBackImage(bitmap, file);
                });
            }
        }
    }


    public int getColorBase(int id) {
        return ContextCompat.getColor(mContext, id);
    }

    public Drawable getDrawableBase(int id) {
        return ContextCompat.getDrawable(mContext, id);
    }

    public void setG(View view, boolean isV) {
        CommonUtils.setG(view, isV);
    }

    public void setV(View view, boolean isV) {
        CommonUtils.setV(view, isV);
    }

    public void loadImg(ImageView imageView, int id) {
        CommonViewUtils.loadImg(imageView, id);
    }

    public void loadImg(ImageView imageView, String url) {
        CommonViewUtils.loadImg(imageView, url);
    }

    protected int getStatusHeight() {
        return StatusBarUtils.getHeight(this);
    }

    /*
     * 系统状态栏是否显示暗色
     * */
    protected void showStatusBarDark(boolean isDark) {
        StatusBarUtils.setDarkStatusIcon(this, isDark);
    }

}
