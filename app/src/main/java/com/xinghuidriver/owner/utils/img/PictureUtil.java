package com.xinghuidriver.owner.utils.img;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import androidx.core.content.FileProvider;


import com.xinghuidriver.owner.config.Constant;
import com.xinghuidriver.owner.utils.CommonUtils;
import com.xinghuidriver.owner.utils.PermissionUtil;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;


/**
 * create by 曹荣冠
 * on 2019/12/24
 */
public class PictureUtil {
    public final static int CAMERA_REQUEST_CODE = 10001;
    public final static int GALLERY_REQUEST_CODE = 10002;
    public final static int MAX_FILE_SIZE = 150 * 1024;
    public final static String APP_FOLDER = "/driver_owner/";
    public final static String fileProvider = Constant.fileProvider;
    public static final String[] cameraAndStorePermissionList = new String[]{PermissionUtil.cameraPermission, PermissionUtil.storePermission, PermissionUtil.storeReadPermission};

    private static File mPhotoFile;
    private final static String TAG = "PictureUtil";

    // 拍照后存储并显示图片
    public static void openCamera(Activity activity, int requestCode) {
        if (!requestCameraPermission(activity, CAMERA_REQUEST_CODE)) {
            return;
        }
        try {
            String mPhotoPath = getAppFolderPath() + getPhotoFileName();//设置图片文件路径，getSDPath()和getPhotoFileName()具体实现在下面
            mPhotoFile = new File(mPhotoPath);
            if (!mPhotoFile.exists()) {
                mPhotoFile.createNewFile();//创建新文件
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                //改变Uri  fileprovider注意和xml中的一致
                //添加权限
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //第二参数是在manifest.xml定义 provider的authorities属性
                intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(activity, fileProvider, mPhotoFile));
            } else {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mPhotoFile));//在这里告诉相机你应该把拍好的照片放在什么位置
            }
            activity.startActivityForResult(intent, requestCode);//跳转界面传回拍照所得数据
        } catch (IOException e) {
            Log.e(TAG, "openCamera " + e.toString());
            e.printStackTrace();
        }
    }

    public static String getAppFolderPath() {
        String folderPath = getSDPath() + APP_FOLDER;
        File file = new File(folderPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return folderPath;
    }

    private static String getPhotoFileName() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("'IMG'_yyyyMMdd_HHmmss");
        return dateFormat.format(date) + ".jpg";
    }

    public static String getSDPath() {
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED);   //判断sd卡是否存在
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();//获取跟目录
        }
        return sdDir.toString();
    }

    public static void openGallery(Activity activity, int requestCode) {
        if (!requestReadPermission(activity, GALLERY_REQUEST_CODE)) {
            return;
        }
        // 激活系统图库，选择一张图片
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(Intent.createChooser(intent, "请选择图片"), requestCode);
    }

    public static void imageResult(Activity activity, int requestCode, Intent data, OnImageCompress compressListener) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            dealCameraResult(activity, compressListener);
        } else if (requestCode == GALLERY_REQUEST_CODE) {
            dealGalleryResult(activity, data, compressListener);
        }
    }

    private static void dealCameraResult(Activity activity, OnImageCompress listener) {
        if (mPhotoFile == null || mPhotoFile.length() == 0) {
            return;
        }
        activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + mPhotoFile.getPath())));
        decodeFileToBitmap(activity, mPhotoFile, listener);
    }

    public static boolean isImageFile(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        return options.outWidth != -1;
    }

    private static void decodeFileToBitmap(Activity activity, final File file, OnImageCompress listener) {
        Bitmap bitmap = null;
        if (file.length() <= MAX_FILE_SIZE) {
            try {
                bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            } catch (Exception error) {
                Log.e(TAG, error.getMessage());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                options.inSampleSize = 2;   //width，hight设为原来的二分一
                bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            } finally {
                if (listener != null) {
                    listener.compressSuccess(bitmap, file);
                }
            }
            return;
        }
        compress(activity, file, listener);
    }

    private static void dealGalleryResult(Activity activity, Intent data, OnImageCompress listener) {
        Uri uri = data.getData();
        if (uri == null) {
            Log.e(TAG, "uri is null");
            return;
        }
        String fileName = getRealFilePath(activity, uri);
        if (isImageFile(fileName)) {
            File file = new File(fileName);
            decodeFileToBitmap(activity, file, listener);
        } else {
            CommonUtils.showToast(activity, "不支持的图片格式，请重新选择图片");
        }

    }


    public static void compress(Context activity, File file, final OnImageCompress listener) {
        Luban.with(activity).load(file).setCompressListener(new OnCompressListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(File file) {
                Bitmap bitmap = null;
                try {
                    bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    if (listener != null) {
                        listener.compressSuccess(bitmap, file);
                    }
                } catch (Error error) {
                    Log.e(TAG, error.getMessage());
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage());
                listener.compressSuccess(null, null);
            }
        }).launch();
    }

    public static String getRealFilePath(final Context context, final Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null) {
            data = uri.getPath();
        } else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }

    /*
     * 判断是否有相机及读写权限
     * */
    public static boolean requestCameraPermission(Activity mContext, int requestCode) {
        //先判断权限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!PermissionUtil.isHasCameraAndStoragePermission(mContext)) {
                mContext.requestPermissions(cameraAndStorePermissionList, requestCode);
                return false;
            }
        }
        return true;
    }

    /*
     * 判断书否有权限
     * */
    public static boolean requestReadPermission(Activity mContext, int requestCode) {
        //先判断权限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!PermissionUtil.isHasReadExternal_storage(mContext)) {
                mContext.requestPermissions(new String[]{PermissionUtil.storeReadPermission}, requestCode);
                return false;
            }
        }
        return true;
    }

    public interface OnImageCompress {
        void compressSuccess(Bitmap bitmap, File file);
    }
}
