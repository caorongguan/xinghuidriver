package com.xinghuidriver.owner.utils;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.xinghuidriver.owner.config.OnRequestSuccess;

import java.util.Calendar;
import java.util.Date;

/**
 * created by caorongguan
 * on 2020/3/25
 */
public class PickerViewUtils {
    public static boolean[] timeTypeList1 = new boolean[]{true, true, true, false, false, false};
    public static boolean[] timeTypeList2 = new boolean[]{true, true, true, false, false, false};

    /**
     * isFuture未来，false 则是指过去。
     */
    public static void init(Context context, boolean isFuture, OnRequestSuccess<String> listener) {
        Calendar selectDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        if (isFuture) {
            endDate.set(2069, 2, 28);
        } else {
            startDate.set(1921, 1, 1);
        }
        TimePickerView pvTime = new TimePickerBuilder(context, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                Log.i("pvTime", "onTimeSelect");
                listener.onResult(CommonUtils.dateToString(date, CommonUtils.DATE_TIME_FORMAT_DAY));
            }
        })
                .setType(timeTypeList1)
                .setDate(selectDate)
                .setRangDate(startDate, endDate)
                .isDialog(true) //默认设置false ，内部实现将DecorView 作为它的父控件。
                .setItemVisibleCount(5) //若设置偶数，实际值会加1（比如设置6，则最大可见条目为7）
                .setLineSpacingMultiplier(2.0f)
                .isAlphaGradient(true).build();

        Dialog mDialog = pvTime.getDialog();
        if (mDialog != null) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
                dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
                dialogWindow.setDimAmount(0.3f);
                mDialog.show();
            }
        }
    }

}