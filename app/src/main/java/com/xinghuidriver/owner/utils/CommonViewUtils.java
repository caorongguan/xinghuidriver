package com.xinghuidriver.owner.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.xinghuidriver.owner.App;
import com.xinghuidriver.owner.R;
import com.xinghuidriver.owner.base.BaseAct;
import com.xinghuidriver.owner.config.OnRequestSuccess;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by caorongguan on 2018/11/9.
 */

public class CommonViewUtils {
    private static ArrayList<TextView> list;
    private static ArrayList<EditText> etList;

    private static boolean isFirst = true;

    public static void logError(Exception e) {
        Log.e("test_commonViewUtils", e.toString());
    }

    public static void setLog(String key, String value) {
        CommonUtils.setLog(key, value + "  ……");
    }

    public static void setLog(String key, List list) {
        CommonUtils.setLog(key, list == null ? "null" : list.size() + "");
    }

    public static void setV(View view, boolean isV) {
        if (view == null) return;
        if (isV) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);
        }
    }

    public static void setGone(View view, boolean isV) {
        if (view == null) return;
        if (isV) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    public static void loadImg(ImageView imageView, Object id) {
        if (!isNull(imageView)) {
            try {
                Glide.with(imageView.getContext()).load(id).into(imageView);
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    public static void loadImgFromPath(ImageView imageView, String path) {
        if (imageView == null) return;
        if (TextUtils.isEmpty(path)) return;
        Bitmap bitmap = getLocalBitmap(path);
        if (bitmap == null) return;
        imageView.setImageBitmap(bitmap);
        Log.e("path_head_img", path + "");
    }

    /**
     * 加载本地图片
     *
     * @param url
     * @return
     */
    public static Bitmap getLocalBitmap(String url) {
        try {
            FileInputStream fis = new FileInputStream(url);
            return BitmapFactory.decodeStream(fis);  ///把流转化为Bitmap图片
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void setImgLogo(ImageView imgLogo) {
        loadImg(imgLogo, R.mipmap.logo);
    }

    public static void loadImg(ImageView imageView, int id) {
        if (!isNull(imageView)) {
            try {
                Glide.with(imageView.getContext()).load(id).into(imageView);
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    /**
     * 加载本地图片到src
     */
    public static void loadImgSrc(ImageView imageView, int id) {
        if (!isNull(imageView)) {
            try {
                imageView.setImageDrawable(getDrawableBase(imageView.getContext(), id));
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    public static void loadImg(ImageView imageView, File file) {
        if (!isNull(imageView)) {
            try {
                Glide.with(imageView.getContext()).load(file).into(imageView);
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    public static RequestOptions getOption() {
        RequestOptions options = new RequestOptions();
        options.skipMemoryCache(false);
        options.diskCacheStrategy(DiskCacheStrategy.ALL);
        options.priority(Priority.HIGH);
        options.error(R.mipmap.icon_img_default);
        //设置占位符,默认
        options.placeholder(R.color.white);
        //设置错误符,默认
        options.error(R.mipmap.icon_img_default);
        return options;
    }

    public static void loadImg(ImageView imageView, String url) {
        url = CommonUtils.ifStringEmpty(url);
        try {
            Context mContext = imageView.getContext();
            // Glide.with(imageView.getContext()).load(url).into(imageView);
            Glide.with(mContext)
                    .asBitmap()
                    .apply(getOption()).load(url)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource,
                                                    @Nullable Transition<? super Bitmap> transition) {
                            imageView.setImageBitmap(resource);
                        }
                    });
        } catch (Exception e) {
            logError(e);
        }
    }

    public static void loadImg1(ImageView imageView, String url) {
        url = CommonUtils.ifStringEmpty(url);
        try {
            // Glide.with(imageView.getContext()).load(url).error(R.mipmap.icon_img_default).dontAnimate().into(imageView);
            Glide.with(imageView.getContext()).load(url).
                    into(imageView);
        } catch (Exception e) {
            logError(e);
        }
    }


    public static void loadImg(ImageView imageView, Bitmap bitmap) {
        if (!isNull(imageView)) {
            try {
                Glide.with(imageView.getContext()).load(bitmap).into(imageView);
            } catch (Exception e) {
                logError(e);
            }

        }
    }

    public static void loadHeadImg(ImageView imageView, String url) {
        url = CommonUtils.ifStringEmpty(url);
        try {
            //Glide.with(imageView.getContext()).load(url).error(R.color.color_f8f8f8).into(imageView);
            Glide.with(imageView.getContext()).load(url).into(imageView);
        } catch (Exception e) {
            logError(e);
        }
    }


    /**
     * 可扩展listView
     * 防止收缩
     */
    public static void cancelClose(ExpandableListView listView, final boolean ifExpandable) {
        //拦截group点击事件，防止收缩
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return ifExpandable;
            }
        });
    }

    /**
     * 展开可扩展的listView
     *
     * @param ifExpandable 是否防止收缩。true 不能收缩。
     */
    public static void openExpandableListView(ExpandableListView listView,
                                              final boolean ifExpandable) {
        int height = getExpandableListViewHeight(listView);
        setViewHeight(listView, height);
        //拦截group点击事件，防止收缩
        cancelClose(listView, ifExpandable);
    }

    public static int getExpandableListViewHeight(ExpandableListView listView) {
        if (listView == null) return 0;
        Context context = listView.getContext();
        int groupCount = listView.getCount();
        for (int i = 0; i < groupCount; i++) {
            listView.expandGroup(i);
        }
        ListAdapter listAdapter = listView.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            //+后面是间距
            totalHeight += listItem.getMeasuredHeight();
            // totalHeight += listItem.getMeasuredHeight() + CommonUtils.dip2px(context, 10);
        }
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + listView.getPaddingTop() + listView.getPaddingBottom() + CommonUtils.dip2px(context, 40);
        return height;
    }

    private static boolean isNull(ImageView imageView) {
        return imageView == null;
    }

    /**
     * 展开listView 列表
     */
    public static int openListView(ListView listView) {
        if (listView == null) return 0;
        Adapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return 0;
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);  // 获取item高度
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // 最后再加上分割线的高度和padding高度，否则显示不完整。
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + listView.getPaddingTop() + listView.getPaddingBottom();
        listView.setLayoutParams(params);
        Log.e("test_height", params.height + "");
        return params.height;
    }

    /**
     * 通用的设置ListView最大高度
     */
    public static int openListViewByNum(ListView listView) {
        int maxCount = 6;
        if (listView == null) return 0;
        Adapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return 0;
        int totalHeight = 0;
        int count = listAdapter.getCount() > maxCount ? maxCount : listAdapter.getCount();
        for (int i = 0; i < count; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);  // 获取item高度
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // 最后再加上分割线的高度和padding高度，否则显示不完整。
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + listView.getPaddingTop() + listView.getPaddingBottom();
        listView.setLayoutParams(params);
        Log.e("test_height", params.height + "");
        return params.height;
    }

    /**
     * @param numColumns 每行的条目数
     */
    public static int openGridView(GridView gridview, BaseAdapter adapter, int numColumns) {
        if (adapter == null) return 0;
        // 固定列宽，有多少列
        int totalHeight = 0;
        // 计算每一列的高度之和
        for (int i = 0; i < adapter.getCount(); i += numColumns) {
            // 获取gridview的每一个item
            View listItem = adapter.getView(i, null, gridview);
            listItem.measure(0, 0);
            // 获取item的高度和
            totalHeight += listItem.getMeasuredHeight() + gridview.getPaddingTop() + gridview.getPaddingBottom()
                    + gridview.getListPaddingTop() + gridview.getListPaddingBottom();
        }
        // 获取gridview的布局参数
        ViewGroup.LayoutParams params = gridview.getLayoutParams();
        params.height = totalHeight;
        gridview.setLayoutParams(params);
        CommonViewUtils.setLog("openGridView", "totalHeight:" + totalHeight);
        return totalHeight;
    }

    /**
     * 展开RecyclerView 列表
     */
    public static int openRecyclerView(RecyclerView listView) {
        if (listView == null) return 0;
        RecyclerView.Adapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return 0;
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getItemCount(); i++) {
            View listItem = listView.getChildAt(i);
            if (listItem != null) {
                listItem.measure(0, 0);  // 获取item高度
                totalHeight += listItem.getMeasuredHeight();
            }
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // 最后再加上分割线的高度和padding高度，否则显示不完整。
        params.height = listView.getPaddingTop() + listView.getPaddingBottom() + totalHeight;
        listView.setLayoutParams(params);
        Log.e("test_height", params.height + "");
        return params.height;
    }


    public static void showPsw(EditText editText, boolean ifShow) {
        if (ifShow) { //显示密码
            editText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {  //隐藏密码
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    public static int getColor(Context context, int id) {
        return ContextCompat.getColor(context, id);
    }

    public static void setViewHeight(View view, int viewHeight) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = viewHeight;
        view.setLayoutParams(params);
        setLog("setViewHeight", viewHeight + "");
    }


    public static void setViewWidth(View view, int viewWidth) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = viewWidth;
        view.setLayoutParams(params);
    }


    /**
     * 将指定text变色。
     */
    public static void changeSomeTextColor(TextView tv, String text) {
        SpannableString newString = new SpannableString(text);
        if (text.contains("/")) {
            int startPosition = text.indexOf("(") + 1;
            int endPosition = text.indexOf("/");
            newString.setSpan(new ForegroundColorSpan(Color.parseColor("#FFE74F18")),
                    startPosition, endPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        tv.setText(newString);
    }

    /**
     * 将指定text变成指定颜色。
     *
     * @param text1 原文字
     * @param text2 需要变色的文字
     *              注：text1中必须只能包含1个text2
     */
    public static void changeTextColor(TextView textView, String text1, String text2, int color) {
        SpannableString newString = new SpannableString(text1);
        if (text1.contains(text2)) {
            int startPosition = text1.indexOf(text1);
            int endPosition = startPosition + text2.length();
            //textView.getResources().getColor(color)
            ForegroundColorSpan newColor = new ForegroundColorSpan(CommonViewUtils.getColorBase(textView.getContext(), color));
            newString.setSpan(newColor, startPosition, endPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setText(newString);
    }

    /**
     * 指定文字变蓝色处理
     *
     * @param textView 需要改变局部文字颜色的TextView
     * @param str      需要变成蓝色的文字。
     */
    public static void changeTextColorBlue(TextView textView, String str) {
        String text = textView.getText().toString();
        Context context = textView.getContext();
        int position = text.indexOf(str);
        if (position < 0) return;
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        //ForegroundColorSpan 为文字前景色，BackgroundColorSpan为文字背景色
        ForegroundColorSpan blueSpan = new ForegroundColorSpan(context.getResources().getColor(R.color.text_blue));
        builder.setSpan(blueSpan, position, position + str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }


    /**
     * 已失效处理 透明度为50%
     */
    public static void setAlpha(TextView textView) {
        if (textView == null) return;
        textView.setTextColor(getColorBase(textView.getContext(), R.color.white_50));
        textView.getBackground().setAlpha(50);
    }


    public static void setText(TextView textView, String text) {
        CommonUtils.setText(textView, text);
    }

    public static void setText(TextView textView, String text, String text2) {
        text = CommonUtils.ifStringEmpty(text);
        text2 = CommonUtils.ifStringEmpty(text2);
        CommonUtils.setText(textView, text + text2);
    }

    public static int getColorBase(Context mContext, int id) {
        return ContextCompat.getColor(mContext, id);
    }

    public static Drawable getDrawableBase(Context mContext, int id) {
        return ContextCompat.getDrawable(mContext, id);
    }

    public static int getContextDp(Context mContext, int id) {
        return mContext.getResources().getDimensionPixelOffset(id);
    }

    public static void setDrawableLeft(TextView view, int id) {
        Drawable drawable = getDrawableBase(view.getContext(), id);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        view.setCompoundDrawables(drawable, null, null, null);
    }

    /**
     * 判断View是否可见
     */
    //当 View 有一点点不可见时立即返回false!
    public static boolean isVisibleLocal(View target) {
        Rect rect = new Rect();
        target.getLocalVisibleRect(rect);
        return rect.top == 0;
    }


    public static void showToast(String str) {
        CommonUtils.showToast(App.getContext(), str);
    }

    public static void showToast(Context context, String str) {
        CommonUtils.showToast(context, str);
    }

    private static String[] scoreDescList = new String[]{"极差", "极差", "较差", "一般", "不错", "很棒"};

    public static void setScoreView(RatingBar bar, TextView textView) {
        if (bar == null) return;
        if (textView == null) return;
        setText(textView, scoreDescList[(int) bar.getRating()]);
        bar.setOnRatingBarChangeListener((RatingBar ratingBar, float rating, boolean fromUser) -> {
            int num = (int) rating;
            setLog("rating_num", num + " " + rating);
            if (num > 0 && num < 6) {
                setText(textView, scoreDescList[num]);
            }
        });
    }


    /**
     * 通用的设置view透明度和可点击
     */
    public static void setViewAlpha(View view, boolean clickAble) {
        if (view == null) return;
        setLog("clickAble", clickAble + "");
        view.setEnabled(clickAble);
        view.setClickable(clickAble);
        float alpha = clickAble ? 1.0f : 0.5f;
        view.setAlpha(alpha);
    }


    public static String getDateStr(String str) {
        str = CommonUtils.ifStringEmpty(str);
        if (str.length() > 10) {
            return str.substring(0, 10);
        }
        return str;
    }

    public static String compareStringDouble(String download, String total) {
        String desc = "";
        String dot = "M";
        double child = subDownloadInt(download);
        double all = subDownloadInt(total);
        if (child > all) {
            child = all;
        }
        desc = child + dot + "/" + all + dot;
        return desc;
    }

    public static String getStrFormList(List<String> list) {
        String desc = "";
        String end;
        if (list != null && list.size() > 0)
            for (int a = 0; a < list.size(); a++) {
                end = a == list.size() - 1 ? "" : "\n";
                desc += CommonUtils.ifStringEmpty(list.get(a)) + end;
            }
        return desc;
    }

    public static double subDownloadInt(String str) {
        double num = 0.0;
        String point = ".";
        try {
            int position = 3;
            if (!TextUtils.isEmpty(str)) {
                if (str.contains(point)) {
                    position = str.indexOf(point);
                    str = str.substring(0, position + 1);
                    num = Double.valueOf(str);
                }
            }
        } catch (Exception e) {
            setLog("subDownloadInt", e.toString());
        }
        return num;
    }

    /**
     * 添加下划线
     */
    public static void plusLine(TextView textView) {
        String text = textView.getText().toString();
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        textView.setText(content);
    }


    /**
     * 检测程序是否安装
     *
     * @param packageName
     * @return
     */
    public static boolean isInstalled(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        //获取所有已安装程序的包信息
        List<PackageInfo> installedPackages = manager.getInstalledPackages(0);
        if (installedPackages != null) {
            for (PackageInfo info : installedPackages) {
                if (info.packageName.equals(packageName))
                    return true;
            }
        }
        return false;
    }

    /**
     * 统一的点击事件的方法
     */
    public static void setOnClick(View view, View.OnClickListener listener) {
        if (view == null) return;
        view.setOnClickListener(v -> {
            if (!CommonUtils.isFastDoubleClick()) {
                listener.onClick(v);
            }
        });
    }

    public static void setTextSize(TextView textView, int id) {
        if (textView == null || id < 0) return;
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, id);
    }

    /**
     * 截取字符串前16位。保留小时分钟
     */
    public static int count = 16;

    public static String getTime(String str) {
        str = CommonUtils.ifStringEmpty(str);
        if (str.length() > count) {
            str = str.substring(0, count);
        }
        return str;
    }

    public static String getYMD(String str) {
        int count = 10;
        str = CommonUtils.ifStringEmpty(str);
        if (str.length() > count) {
            str = str.substring(0, count);
        }
        return str;
    }

    public static String getYM(String str) {
        int count = 7;
        str = CommonUtils.ifStringEmpty(str);
        if (str.length() > count) {
            str = str.substring(0, count);
        }
        return str;
    }

    public static String getMonthDay(String str) {
        if (!TextUtils.isEmpty(str) && str.length() >= 10) {
            str = str.substring(5, 10);
            if (str.startsWith("0")) {
                str = str.substring(1, 5);
            }
        }
        setLog("getMonthDay", str);
        return str;
    }


    public static void addTextWatch(EditText et, OnRequestSuccess<String> listener) {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (listener != null) listener.onResult(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

}
