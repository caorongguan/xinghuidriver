package com.xinghuidriver.owner.adapter;


import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.xinghuidriver.owner.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caorongguan on 2019/7/25.
 */

public class HomePageAdapter extends FragmentStatePagerAdapter {
    private List<BaseFragment> fragments;

    public HomePageAdapter(FragmentManager fm, List<BaseFragment> lists) {
        super(fm);
        fragments = new ArrayList<>();
        fragments.addAll(lists);
    }

    @Override
    public BaseFragment getItem(int position) {
        return fragments.get(position);

    }

    @Override
    public int getCount() {
        return fragments==null?0:fragments.size();
    }
}
