package com.xinghuidriver.owner.ui.activity;


import android.widget.FrameLayout;

import com.xinghuidriver.owner.R;
import com.xinghuidriver.owner.base.BaseAct;
import com.xinghuidriver.owner.ui.fragment.MainFragment;

public class MainActivity extends BaseAct {
    private MainFragment mainFragment;

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void initViews() {
        mainFragment = new MainFragment();
        jumpToFragment(mainFragment, R.id.frameLayout);
        setCommonTitle("sss");
    }

}