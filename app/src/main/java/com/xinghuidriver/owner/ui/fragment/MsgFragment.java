package com.xinghuidriver.owner.ui.fragment;

import com.xinghuidriver.owner.R;
import com.xinghuidriver.owner.base.BaseFragment;

/**
 * @Author : caorongguan
 * @Time : On 2021/6/14 23:06
 * @Description : MsgFragment
 */
public class MsgFragment extends BaseFragment {
    @Override
    public int getLayout() {
        return R.layout.fragment_msg;
    }

    @Override
    public void initView() {

    }
}
