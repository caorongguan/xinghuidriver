package com.xinghuidriver.owner.ui.fragment;

import com.xinghuidriver.owner.base.BaseFragment;
import com.xinghuidriver.owner.base.HomeBasePagerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : caorongguan
 * @Time : On 2021/6/14 22:42
 * @Description : HomeFragment
 */
public class MainFragment extends HomeBasePagerFragment {
    List<BaseFragment> list = new ArrayList();

    @Override
    protected List<BaseFragment> createFragments() {
        list.clear();
        list.add(new HomeFragment());
        list.add(new OrderFragment());
        list.add(new MsgFragment());
        return list;
    }

}
