package com.xinghuidriver.owner.config;


/**
 * Created by caorongguan on 2018/11/30.
 */

public interface OnRequestSuccess<T>{
    void onResult(T result) ;
}
