package com.xinghuidriver.owner.config;

import com.xinghuidriver.owner.R;

/**
 * Created by caorongguan on 2019/5/6.
 */

public class Constant {
    // 主页面导航菜单
    public final static String digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public final static String idCardDigits = "0123456789xX";
    public final static String fileProvider = "com.xinghuidriver.owner.fileprovider";
    public final static String url = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1576747094827&di=e4cb3e1224bf9891a50768b02f9c24f9&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201806%2F28%2F2018062880925_TLjik.jpeg";

    public static final String[] OWNER_TITLE = {"首页", "订单", "消息"};
    public static final int[] OWNER_ICONS = {R.drawable.selector_home_table, R.drawable.selector_home_table,
            R.drawable.selector_home_table};
    public static String token = "";
    // request参数
    public static final int REQ_QR_CODE = 11002; // // 打开扫描界面请求码
    public static final int REQ_PERM_CAMERA = 11003; // 打开摄像头
    public static final int REQ_PERM_EXTERNAL_STORAGE = 11004; // 读写文件

    public static final String INTENT_EXTRA_KEY_QR_SCAN = "qr_scan_result";
    public static final int REQUEST_CODE = 100;


}
